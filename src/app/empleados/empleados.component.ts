import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { EmpleadosService } from '../services/empleados.service';

export interface EmpleadoElement {
  id: number;
  fecha_ingreso: string;
  nombre: string;
  salario: number;
}

export interface DialogData {
  id: number;
  fecha_ingreso: string;
  nombre: string;
  salario: number;
}

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {
  displayedColumns: string[] = ['id', 'fecha_ingreso', 'nombre', 'salario'];
  dataSource: any;
  employees: any;

  nombre!: string;
  fecha_ingreso!: string;
  salario!: string;

  private subscription: Subscription = new Subscription();

  constructor(
    private empleadosService: EmpleadosService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getEmpleados();
  }

  getEmpleados(): void {
    this.subscription.add(
      this.empleadosService.getEmpleados().subscribe((res) => {
        if (res.status === 200) {
          this.employees = res.data;
          this.dataSource = this.employees;
        } else {
          this.employees = [];
          // this.notificationsComponent.showNotification(res.status, res.msg)
        }
      })
    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {nombre: this.nombre, fecha_ingreso: this.fecha_ingreso, salario: this.salario}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result)

      this.subscription.add(
        this.empleadosService.postEmpleados(result).subscribe((res) => {
          alert('se creo correctamente')
        })
      )
    });
  }

} 

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-data-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}