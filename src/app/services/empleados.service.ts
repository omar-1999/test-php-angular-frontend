import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { environment } from 'environments/environment';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface EmployeesResponse {
  status: number;
  title?: string;
  msg?: string;
  data?: string;
}

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {
  // Configurar headers
  headers = new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
  });

  constructor(private http: HttpClient) { }

  getEmpleados(): Observable<EmployeesResponse> {
    return this.http.get<EmployeesResponse>(`${environment.API_URL}empleado`, {})
      .pipe(
        map((res: EmployeesResponse) => {
          return res;
        }),
        catchError((err) => this.handlerError(err))
      );
  }

  postEmpleados(data: any) {
    return this.http.post(`${environment.API_URL}empleado`, data, {
      headers: this.headers,
    }).pipe(
      map((res: {}) => {
        return res;
      }), catchError((err) => this.handlerError(err))
    );
  }

  // Controlar errores  de inicio de sesión
  handlerError(err: any): Observable<never> {
    let errorValue = false;
    if (err) {
      errorValue = true;
    }
    return throwError(errorValue);
  }
}
